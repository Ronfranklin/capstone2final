<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::delete('/cart/clear', 'CartController@clear')->name('cart.clear');
Route::resource('order','OrderController');
Route::resource('products','ProductController');
Route::resource('categories','CategoryController');
Route::resource('branch','BranchController');
Route::resource('transaction','TransactionController');
Route::resource('cart','CartController');

Route::get('/home', 'HomeController@index')->name('home');
