<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

	public function status()
	{
		return $this->belongsTo('App\Status');
	}

	public function products()
	{
		return $this->belongsToMany('App\Product','order_product')->withPivot('quantity')->withTimestamps();
	}

	public function user(){
		return $this->belongsTo('App\User');
	}
}
