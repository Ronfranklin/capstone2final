<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Branch;
use Illuminate\Http\Request;
use Str;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('products.index')->with('products', $products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $branches = Branch::all();
        return view('products.create')->with('categories', $categories)->with('branch', $branches);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->authorize('create', $products);
        // Flow
            // 1. Validate each field (validation errors will display in views)
            // 2. save uploaded image (to get the path which will be save to the database)
            // 3. get input values from form
            // 4. save the details to the database
            // 5. redirect user to view the newly created product

        //==============================================
        // 1. Validate each field (validation errors will display in views)
        $request->validate([
            'name' => 'required|string',
            'category_id' => 'required',
            'image' => 'required|image',
            'stock' => 'required|numeric',
            'description' => 'required|string'
        ]);


        //==============================================
        // 2. save uploaded image (to get the path which will be save to the database)
            // 2.1 get uploaded file
            // 2.2 get uploaded filename
            // 2.3 get uploaded file extension name
            // 2.4 generate new name with random characters
            // 2.5 save the new file to public folder

            //====================================
            // 2.1 get uploaded file

        $category_code = Category::find($request->input('category_id'));

        $file = $request->file('image');
            // dd($file);

            // 2.2 get uploaded filename
        $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

            // 2.3 get uploaded file extension name
        $file_extension = $file->extension();

            // 2.4 generate new name with random characters
        $random_chars = Str::random(10);


        $final_code = $category_code->name . "_" . date('Y-m-d-H-i-s') . $random_chars;
        $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." .  $file_extension;


        // if($category_code-> == 0){
        //     return $stockfile = true;
        // }else{
        //     return $stockfile = false;
        // }

            // 2.5 save the new file to public folder
        $filepath =  $file->storeAs('images',$new_file_name, 'public' );


        //==============================================
             // 3. get input values from form
        $name = $request->input('name');
        $category_id = $request->input('category_id');
        $image = $filepath;
        $stock = $request->input('stock');
        $code = $final_code;
        $description = $request->input('description');

        //==============================================
            // 4. save the details to the database
        $product = new Product;
        $product->name =$name;
        $product->category_id =$category_id;
        $product->image = $filepath;
        $product->stock =$stock;
        $product->code = $final_code;
        $product->description =$description;
        $product->save();

            // 5. redirect user to view the newly created product
        return redirect( route('products.show', ['product' => $product->id] ));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('products.show')
        ->with("product", $product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $categories = Category::all();
        $branches = Branch::all();
        return view('products.edit', ['product' => $product])
        ->with("categories", $categories)
        ->with("branches", $branches);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        // Flow
            // 1. Validate the form, check if all field are not empty
            // 2. Update database if there are differences between the detials form form and the current detials of the product
        // ==============================
            // 1. Validate the form, check if all field are not empty
        $request->validate([
            'name' => 'required|string',
            'category_id' => 'required',
            'image' => 'required|image',
            'stock' => 'required|numeric',
            'description' => 'required|string'
        ]);


    // 2. Update database if there are differences between the detials form form and the current details 
        if(
            !$request->hasFile('image') && 
            $product->name == $request->input('name') &&
            $product->category_id == $request->input('category_id')&&
            $product->stock == $request->input('$stock') &&
            $product->description == $request->input('description')
            
        ){
            $request->session()->flash('update_failed','Something went wrong!');
            
        }else{
            // update the entry in the database and return the updated entry

            // check if there is a file uploaded
            if($request->hasFile('image')){
                $request->validate([
                    'image' => 'required|image',
                ]);

                // 2.1 get uploaded file
                $file = $request->file('image');
                // dd($file);

                // 2.2 get uploaded filename
                $file_name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

                // 2.3 get uploaded file extension name
                $file_extension = $file->extension();

                // 2.4 generate new name with random characters
                $random_chars = Str::random(10);
                $new_file_name = date('Y-m-d-H-i-s') . "_" . $random_chars . "_" . $file_name . "." .  $file_extension;

                // 2.5 save the new file to public folder
                $filepath =  $file->storeAs('images',$new_file_name, 'public' );

                // set the new image path as image value of product
                $product->image=$filepath;

            }

            // update the product attribute
            $product->name =$name;
            $product->category_id =$category_id;
            $product->image = $filepath;
            $product->stock =$stock;
            $product->code = $final_code;
            $product->description =$description;

            $product->save();

            $request->session()->flash('update_sucess','Product Successfully Update');

        }

        return redirect( route('products.edit',['product' => $product->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        // $request->session()->flash('destroy_sucess','Product Deleted');
        return redirect(route('products.index'))->with('destroy_message','Product Deleted');
    }
}
