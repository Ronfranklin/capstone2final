@extends('layouts.app')

@section('content')



<div class="container">
	<div class="row">
		<div class="col-8 col-md-8 mx-auto">
			<h3 class="text-center">Edit Product</h3>
			<hr>

			<form action="{{ route('products.store') }}" method="post" enctype="multipart/form-data">
				@csrf

				{{-- name --}}
				<div class="form-group">
					<label for="name">Product Name: </label>
					<input type="text" name="name" class="form-control" id="name" value="{{$product->name}}">
				</div>
				@if($errors->has('name'))
				<div class="alert alert-danger">
					<small class="mb-0">Product name Required</small>
				</div>
				@endif

				{{-- category --}}
				<div class="form-group">
					<label for="category_id">Category</label>
					<select class="form-control" id="category_id" name="category_id">
						@foreach($categories as $category)
						<option value="{{ $category->id}}">{{$category->name}}</option>
						@endforeach
					</select>
				</div>
				@if($errors->has('category'))
				<div class="alert alert-danger">
					<small class="mb-0">Product category required</small>
				</div>
				@endif

				{{-- branch --}}
				<div class="form-group">
					<label for="category_id">Branch</label>
					<select class="form-control" id="category_id" name="category_id">
						@foreach($branches as $branch)
						<option value="{{ $branch->id}}">{{$branch->name}}</option>
						@endforeach
					</select>
				</div>
				@if($errors->has('category'))
				<div class="alert alert-danger">
					<small class="mb-0">Product category required</small>
				</div>
				@endif

				{{-- image --}}
				<div class="form-group">
					<label for="image">Image:  </label>
					<input type="file" name="image" class="form-control-file" id="image">
				</div>
				@if($errors->has('image'))
				<div class="alert alert-danger">
					<small class="mb-0">Product image required, Check your image file</small>
				</div>
				@endif

				{{-- quantity and mass --}}
				<div class="form-group">
					<label for="stock">Stocks: </label>
					<input class="form-control" min="0" name="stock" value="{{$product->stock}}" type="number">
				</div>
				@if($errors->has('stock'))
				<div class="alert alert-danger">
					< class="mb-0">Product Stocks required</small>
				</div>
				@endif

				{{-- description --}}
				<div class="form-group">
					<label>Description: </label>
					<textarea name="description" class="form-control" cols="30" rows="5" id="description">{{$product->description}}</textarea>
				</div>
				@if($errors->has('description'))
				<div class="alert alert-danger">
					<small class="mb-0">Product description required,</small>
				</div>
				@endif


				<button class="btn btn-outline-primary w-100">Update Item</button>

				
			</div>

		</form>
		

		
</div>
		
	</div>

</div>

@endsection