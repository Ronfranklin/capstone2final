@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12">
			<h3 class="text-center">Your Order Details</h3>
		</div>{{-- end of details div --}}
	</div>{{-- end row --}}
	<div class="row">
		<div class="col-12 col-md-10 mx-auto">
			{{-- @if(Session::has('order') $$ count(Session::get('order'))) --}}
			<div class="table-responsive">
				<table class="table table-hover">
					<thead class="thead-dark text-center">
						<th scope="col">Name: </th>
						<th scope="col">Stock: </th>
						<th scope="col">Number of order</th>
						<th scope="col">Total: </th>
						<th scope="col">Action: </th>
					</thead>{{-- end of thead --}}
					<tbody>
						@foreach($products as $product)
						<tr>
							<td>
								<p class="card-text text-center">{{$product->name}}</p>
								<img src="{{ url('/public/' .$product->image) }}" class="img-fluid ">
							</td>
							<td><span class="product-stock">{{number_format($product->stock)}}</span></td>
							<td>


								<form action="{{ route('cart.store') }}" method="POST" class="add-to-cart-field">
									@csrf

									<div class="form-group">
										<input type="hidden" name="id" value="{{$product->id}}">
										<div class="input-group">

											<div class="input-group-prepend">
												<button class="btn btn-outline-secondary deduct-quantity" 
												data-id= "{{ $product->id}}" type="button" >-</button>
											</div>

											<input
											type="text" 
											name="stock"
											class="form-control input-quantity" 
											placeholder="Enter Quantity" 
											data-id= "{{$product->id}}"
											value="{{$product->stock}}" 
											>

											<div class="input-group-append">
												<button class="btn btn-outline-secondary add-quantity" 
												data-id= "{{$product->id}}" type="button">+</button>
											</div>
										</div>
									</div>
									<button type="submit" class="btn btn-outline-success my-1 w-100 ">Update</button>
								</form>
							</td>
							<td><span class="product-subtotal">{{number_format($product->stock)}}</span></td>
							<td>
								<form action="{{ route('cart.destroy', ['cart' => $product->id]) }}" method="post">
									@csrf
									@method('DELETE')
									<button class="btn btn-outline-danger">Remove From Cart</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</div>
				<tr>
					<td colspan="5">
						<form action="{{ route('order.store') }}" method="post">
							@csrf
							<button class="btn btn-primary w-100">Confirm Products</button>
							<hr>
						</form>

						<form action="{{ route('cart.clear') }}" method="post">
							@csrf
							@method('DELETE')
							<button class="btn btn-danger w-100">Empty Product</button>
						</form>

					</td>
				</tr>
			</tfoot>
		</table>{{-- end of table --}}
	</div>
	{{-- @endif --}}
</div>{{-- row for product --}}
</div>
</div>{{-- end container --}}

</div>
@endsection