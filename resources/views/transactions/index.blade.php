@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-12 col-md-8 mx-auto">
			<h3 class="text-center">Transactions</h3>
		</div>{{-- end of header col --}}

		<div class="col-12 col-md-8 mx-auto">
			<table class="table table-dark table-hover">
				<thead class="thead-dark">
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Name</th>
						<th scope="col" class="text-center">Order Number</th>
						<th scope="col">Quantity</th>
						<th scope="col">Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($products as $product)
					@foreach($users as $user)
					<tr>
						<th scope="row">{{$product->id}}</th>
						<td>{{$product->user->name}}</td>
						<td class="text-center">{{$product->order_number}}</td>
						<td>{{$product->quantity}}</td>
						<td>@mdo</td>
						@endforeach
						@endforeach
					</tr>
				</tbody>
			</table>
		</div>
	</div>{{-- end of row --}}
</div>{{-- end of container --}}

@endsection

