<?php

use Illuminate\Database\Seeder;

class BranchTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branches')->insert([
     	'name'=> 'Branch 1',
     	'address'=> 'Quezon City'
     	]);

     	DB::table('branches')->insert([
     	'name'=> 'Branch 2',
     	'address'=> 'Manila City'
     	]);

     	DB::table('branches')->insert([
     	'name'=> 'Branch 3',
     	'address'=> 'Pasig City'
     	]);
    }
}
