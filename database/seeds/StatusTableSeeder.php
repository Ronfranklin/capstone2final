<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('statuses')->insert([
        'name'=> 'pending'
        ]);

        DB::table('statuses')->insert([
     	'name'=> 'out of stock'
     	]);

        DB::table('statuses')->insert([
     	'name'=> 'delivery on process'
     	]);

     	DB::table('statuses')->insert([
     	'name'=> 'received'
     	]);

     	DB::table('statuses')->insert([
     	'name'=> 'failed'
     	]);
    }
}
