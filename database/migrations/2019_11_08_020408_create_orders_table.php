<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->string('order_number');

            // user
           $table->unsignedBigInteger('user_id');
           $table->foreign('user_id')
           ->references('id')->on('users')
           ->onUPDATE('cascade')
           ->onDELETE('restrict');

           //  //branch
           // $table->unsignedBigInteger('branch_id');
           // $table->foreign('branch_id')
           // ->references('id')->on('branches')
           // ->onUPDATE('cascade')
           // ->onDELETE('restrict');
           
            //status_id
           $table->unsignedBigInteger('status_id')->default(1);
           $table->foreign('status_id')
           ->references('id')->on('statuses')
           ->onUPDATE('cascade')
           ->onDELETE('restrict');
           
           $table->softDeletes();
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
