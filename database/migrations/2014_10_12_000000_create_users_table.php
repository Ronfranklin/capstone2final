<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
        $table->bigIncrements('id');
        $table->string('name');
        $table->string('email')->unique();
        $table->timestamp('email_verified_at')->nullable();
        $table->string('password');
        $table->rememberToken();
        $table->timestamps();

        $table->unsignedBigInteger('role_id')->default(2);
        $table->foreign('role_id')
        ->references('id')->on('roles')
        ->onDELETE('restrict')
        ->onUPDATE('cascade');

        $table->unsignedBigInteger('branch_id');
        $table->foreign('branch_id')
        ->references('id')->on('branches')
        ->onDELETE('restrict')
        ->onUPDATE('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
